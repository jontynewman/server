CREATE TABLE "album" (
	"id" TEXT NOT NULL,
	"title" TEXT NOT NULL,
	"release" DATETIME,
	"artwork" TEXT,
	FOREIGN KEY ("artwork") REFERENCES "image"("id"),
	PRIMARY KEY ("id")
);

CREATE TABLE "audio" (
	"id" TEXT NOT NULL,
	"title" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "album_track" (
	"album" TEXT NOT NULL,
	"track" TEXT NOT NULL,
	"order" INTEGER NOT NULL,
	FOREIGN KEY ("album") REFERENCES "album"("id"),
	FOREIGN KEY ("track") REFERENCES "audio"("id")
);

CREATE TABLE "compilation" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "compilation_video" (
	"compilation" TEXT NOT NULL,
	"video" TEXT NOT NULL,
	FOREIGN KEY ("compilation") REFERENCES "compilation"("id"),
	FOREIGN KEY ("video") REFERENCES "video"("id"),
	PRIMARY KEY ("compilation", "video")
);

CREATE TABLE "gallery" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "gallery_image" (
	"gallery" TEXT NOT NULL,
	"image" TEXT NOT NULL,
	FOREIGN KEY ("gallery") REFERENCES "gallery"("id"),
	FOREIGN KEY ("image") REFERENCES "image"("id"),
	PRIMARY KEY ("gallery", "image")
);

CREATE TABLE "image" (
	"id" TEXT NOT NULL,
	"alt" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "page" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "page_type_page" (
	"page_type" TEXT NOT NULL,
	"page" TEXT NOT NULL,
	FOREIGN KEY ("page_type") REFERENCES "page_type"("id"),
	FOREIGN KEY ("page") REFERENCES "page"("id"),
	PRIMARY KEY ("page_type")
);

CREATE TABLE "page_url" (
	"url" TEXT NOT NULL,
	"page" TEXT NOT NULL,
	FOREIGN KEY ("page") REFERENCES "page"("id"),
	PRIMARY KEY ("url")
);

CREATE TABLE "page_type" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "permission" (
	"id" TEXT NOT NULL,
	"description" TEXT,
	PRIMARY KEY ("id")
);

CREATE TABLE "permission_group" (
	"id" TEXT NOT NULL,
	"description" TEXT,
	PRIMARY KEY ("id")
);

CREATE TABLE "permission_group_permission" (
	"permission_group" TEXT NOT NULL,
	"permission" TEXT NOT NULL,
	FOREIGN KEY ("permission_group") REFERENCES "permission_group"("id"),
	FOREIGN KEY ("permission") REFERENCES "permission"("id"),
	PRIMARY KEY ("permission_group", "permission")
);

CREATE TABLE "role" (
	"id" TEXT NOT NULL,
	"description" TEXT,
	PRIMARY KEY ("id")
);

CREATE TABLE "role_permission_group" (
	"role" TEXT NOT NULL,
	"permission_group" TEXT NOT NULL,
	FOREIGN KEY ("role") REFERENCES "role"("id"),
	FOREIGN KEY ("permission_group") REFERENCES "permission_group"("id"),
	PRIMARY KEY ("role", "permission_group")
);

CREATE TABLE "script" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "setting" (
	"id" TEXT NOT NULL,
	"value" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "stylesheet" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "super_user" (
	"user" TEXT NOT NULL,
	FOREIGN KEY ("user") REFERENCES "user"("id"),
	PRIMARY KEY ("user")
);

CREATE TABLE "user" (
	"id" TEXT NOT NULL,
	"password" TEXT NOT NULL,
	"salt" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

CREATE TABLE "user_role" (
	"user" TEXT NOT NULL,
	"role" TEXT NOT NULL,
	FOREIGN KEY ("user") REFERENCES "user"("username"),
	FOREIGN KEY ("role") REFERENCES "role"("name"),
	PRIMARY KEY ("user", "role")
);

CREATE TABLE "video" (
	"id" TEXT NOT NULL,
	PRIMARY KEY ("id")
);

INSERT INTO "page" ("id")
VALUES ('header'), ('nav'), ('footer'), ('home'), ('cv');

INSERT INTO "page_type" ("id")
VALUES ('header'), ('nav'), ('footer');

INSERT INTO "page_type_page" ("page_type", "page")
VALUES ('header', 'header'),
('nav', 'nav'),
('footer', 'footer');

INSERT INTO "page_url" ("url", "page")
VALUES ('/', 'home'), ('/cv', 'cv');

INSERT INTO "stylesheet" ("id")
VALUES ('stylesheet');

INSERT INTO "setting" ("id", "value")
VALUES ('title', 'Jonty Newman');

