Bash scripts for operating [jontynewman.com](http://jontynewman.com/).

To launch the website, execute the following command from the root of this
project as a priviledged user on an [Arch Linux](https://www.archlinux.org/)
installation (replacing `user` with a non-priviledged user).

`./launch.sh user ./uploads < ./jontynewman.com.sql`

Each script performs a specific set of operations.

`launch.sh`
: Installs, configures and runs the web server (and attempts to revert all
actions if a failure occurs).

`install.sh`
: Installs all required packages.

`config.sh`
: Configures the web server and all related dependencies.

`run.sh`
: Enables and starts the web server.

`ddclient.sh`
: Installs, configures and runs ddclient (with the password given as the first
argument). It is independent of `launch.sh`.

`undo.*.sh`
: Reverts the actions of the associated script.

