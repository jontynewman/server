# Warnings relating to journal may appear (https://github.com/systemd/systemd/issues/5607).

read -r -d '' NGINX_CONF <<'EOF'
d /run/nginx 0775 root http - -
EOF

read -r -d '' USER_CONF <<'EOF'
[Service]
User=http
Group=http
NoNewPriviledges=yes
CapabilityBoundingSet=
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
AmbientCapabilities=
AmbientCapabilities=CAP_NET_BIND_SERVICE
PIDFile=/run/nginx/nginx.pid
ExecStart=
ExecStart=/usr/bin/nginx -g 'pid /run/nginx/nginx.pid; error_log stderr;'
EOF

echo "$NGINX_CONF"  > /etc/tmpfiles.d/nginx.conf \
&& systemd-tmpfiles --create \
&& mkdir /etc/systemd/system/nginx.service.d \
&& echo "$USER_CONF" > /etc/systemd/system/nginx.service.d/user.conf \
&& rm -rf /var/lib/nginx/* \
&& nginx -t \
&& rm -rf /var/log/nginx/* \
&& chmod ug=rwx,o= /var/log/nginx \
&& chgrp http /var/log/nginx \
&& systemctl daemon-reload \
&& systemctl enable php-fpm.service \
&& systemctl enable nginx.service \
&& systemctl start php-fpm.service \
&& systemctl start nginx.service

