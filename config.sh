read -r -d '' COMPOSER_JSON <<'EOF'
{
	"require": {
		"shroophp/cms": "dev-master",
		"shroophp/framework": "dev-master"
	}
}
EOF

read -r -d '' INDEX_PHP <<'EOF'
<?php

use ShrooPHP\Framework\CallableObject;
use ShrooPHP\Framework\Database\ConnectionPool;
use ShrooPHP\Framework\Database\Connections\PdoConnection;
use ShrooPHP\Framework\Helpers\HtmlHelper;
use ShrooPHP\Framework\Requests\PhpRequest;
use ShrooPHP\Framework\Templates\HtmlTemplate;
use ShrooPHP\Framework\Views\View;
use ShrooPHP\Framework\Viewers\PhpViewer;
use ShrooPHP\CMS\Configuration;

require '/root/jontynewman.com/vendor/autoload.php';

$html = null;

$viewer = new PhpViewer();

$db = new PdoConnection('sqlite:/root/jontynewman.com/jontynewman.com.db');

$options = array(
	          Configuration::VIEWER => $viewer,
	       Configuration::REDIRECTS => true,
	Configuration::UPLOAD_DIRECTORY => '/usr/share/nginx/html/uploads',
	           Configuration::ALBUM => '/music/',
	         Configuration::GALLERY => '/images/',
	     Configuration::COMPILATION => '/videos/'
);

$cms = new Configuration($db, $options);

$request = new PhpRequest($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

// Run
if (!$cms->handle($request)) {

	$html = new CallableObject(function() {

		$html = new HtmlHelper();

		echo $html->tag('h1');
		echo $html->encode('Not Found');
		echo $html->close('h1');
		
		echo $html->tag('p');
		echo $html->encode('The requested resource could not be found');
		echo $html->encode(' but may be available again in the future.');
		echo $html->close('p');
	});

	$viewer->view(new HtmlTemplate(404, null, 'Not Found', $html));
}
EOF

read -r -d '' NGINX_CONF <<'EOF'
worker_processes auto;
worker_cpu_affinity auto;
pcre_jit on;

events {
    worker_connections 2048;
}

http {
    include mime.types;
    default_type application/octet-stream;
    sendfile on;
    tcp_nopush on;
    aio threads;
    server_tokens off;
    charset utf-8;
    index index.php;
    
    server {
        listen 80;
        server_name jontynewman.com;
        root /usr/share/nginx/html;
	rewrite .* /index.php last;
        location ~ \.php$ {
             fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
             fastcgi_index index.php;
             include fastcgi.conf;
        }

        gzip on;
        gzip_types application/javascript text/css text/plain;
    }
}
EOF


PHP_INI=$(sed 's/^;\(extension=\(pdo_sqlite\.so\|sqlite3\.so\)\)$/\1/' /etc/php/php.ini) \
&& echo "$PHP_INI" > /etc/php/php.ini \
&& mkdir /root/jontynewman.com \
&& sqlite3 /root/jontynewman.com/jontynewman.com.db < /dev/stdin \
&& echo "$COMPOSER_JSON"  > /root/jontynewman.com/composer.json \
&& chmod u+w /root/jontynewman.com \
&& chown $1 /root/jontynewman.com \
&& chgrp $1 /root \
&& su -c 'composer install -d /root/jontynewman.com -n -o' $1 \
&& chgrp http /root \
&& echo "$INDEX_PHP" > /usr/share/nginx/html/index.php \
&& find /root/jontynewman.com/vendor -exec chmod ug=rx,o= {} + -exec chown http:http {} + \
&& chmod ug=rwx,o= /root/jontynewman.com /usr/share/nginx/html \
&& chmod ug=rw,o= /root/jontynewman.com/jontynewman.com.db \
&& chmod ug=rx,o= /usr/share/nginx/html/index.php \
&& chown http:http /root/jontynewman.com /root/jontynewman.com/jontynewman.com.db /usr/share/nginx/html /usr/share/nginx/html/index.php \
&& cp -r $2 /usr/share/nginx/html/uploads \
&& find /usr/share/nginx/html/uploads -type d -exec chmod ug=rwx,o= {} + \
&& find /usr/share/nginx/html/uploads -type f -exec chmod ug=r,o= {} + \
&& find /usr/share/nginx/html/uploads -exec chown http:http {} + \
&& cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak \
&& echo "$NGINX_CONF" > /etc/nginx/nginx.conf

