read -r -d '' CONF <<EOF
daemon=300
syslog=yes
mail=root
mail-failure=root
pid=/var/run/ddclient.pid
ssl=yes
use=web, web=http://ipv4.wtfismyip.com/text
server=www.duckdns.org
password=$1
protocol=duckdns
jontynewman
EOF

pacman -Syu --noconfirm ddclient \
&& mv /etc/ddclient/ddclient.conf /etc/ddclient/ddclient.conf.bak \
&& echo "$CONF" > /etc/ddclient/ddclient.conf \
&& systemctl enable ddclient.service \
&& systemctl start ddclient.service

