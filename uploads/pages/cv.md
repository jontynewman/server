## Profile

Disciplined, self-motivated and diligent Software Engineering graduate of Aberystwyth University with a strong ability for designing and implementing systems using various programming languages. Experienced in collaborating with others in order to build and maintain solutions. Now seeking to further career by working on technical products in an agile team.


## Key Achievements and Skills

- Responsible for implementing numerous features for internal tools at Holidaysplease Ltd, the most sophisticated of which being a system for retrieving live flight prices for multiple destinations and departure dates simultaneously. The project built on my existing knowledge of web APIs and expanded my understanding of the challenges involved in implementing concurrent algorithms.

- Maintained and extended a web application used by developers at BlackBerry Ltd during an internship to assist in the development of their 3G protocol stack. The application tracked tasks that were assigned to certain developers, informed relevant managers of submissions and automated	approvals for the configuration management system (Telelogic Synergy).

- Adept in PHP (including Laravel), Java, JavaScript (including jQuery and Vue.js), C/C++, Ruby (including Ruby on Rails), Python, MySQL (including MariaDB), SQLite, PostgreSQL, SOAP, XML, HTML5, CSS, LaTeX and Git. I am exceptionally passionate about each of these and thoroughly enjoy applying these skills in my work.

- Worked collaboratively with a fellow colleague at Holidaysplease Ltd to migrate a legacy system in order for it to adopt more modern practices. This project was significant for developing existing skills in the latest tools as well as in the challenges involved in such a task.

- Formal experience in developing and using RESTful web APIs in order for two isolated systems to communicate with one another. This includes multiple flight service API clients for Holidayplease Ltd, and an API for an internal configuration management server at BlackBerry Ltd.


## Education

### 04/2015 - Aberystwyth University

- Software Engineering BEng
- Outcome: First Class
- Key modules:
  - Professional Issues In The Computing Industry
  - Developing Internet-Based Applications
  - C and Unix Programming
  - Web Programming
  - Agile Methodologies
- Dissertation topic: Secure Monitoring of Remote Systems (C++)

  <https://github.com/CentreForAlternativeTechnology/SensorGateway/>

  The aim of the project was to use a microcomputer as a gateway in order to convert encrypted RF24 radio messages to HTTPS messages (and vice versa). This would provide communications between a remote sensor and internal systems at the Centre for Alternative Technology (<http://cat.org.uk/>). It required collaboration with fellow students as their projects formed parts of the system. Clear communication and up-to-date documentation were key to the project's success.

### 09/2011 - Hagley RC High School

- A Levels: B (Computer Science), C (Maths), C (English Literature)
- AS Levels: C (History)
- 11 GCSEs, including:
  - A* (Higher Mathematics)
  - Distinction (Certificate in Digital Applications Level 2)
  - Merit (Award in Digital Applications Level 2)


## Work Experience

### 06/2015 - Holidaysplease Ltd

- One major project involved adding flights to an existing hotel pricing system. It is capable of calculating the cheapest price for multiple destinations, durations and room combinations practically instantaneously. To maintain this performance, exploiting concurrency was crucial for retrieving  flight prices. This was my first experience in implementing a system of this level of complexity. It was important to rely on the expertise and knowledge of my superior, which highlighted the importance of learning from others.

- Early on in this position, my main responsibility was designing and implementing a number of features for the Holidaysplease Extranet under the supervision of the IT Lead. The primary goal was to automate a number of business processes so that holiday bookings can be handled more efficiently. Therefore, it was necessary to communicate openly and frequently with colleagues and superiors. This established the needs of the users and the company, and hence ensured that only valued solutions were produced.

- During this period, the team transitioned from using an ad-hoc methodology to one that incorporated more agile principles, following a proposal put forward by myself. It was enlightening to witness the benefits gained through the adoption of these changes, as it provided much clearer expectations between developers, supervisors and stakeholders alike.

### 07/2013 - BlackBerry Ltd

- Maintained a web application designed to interface between developers, the configuration management system and the relevant managers. I also carried out work that extended the functionality of the application, the most significant of which was a tool that could determine which tasks had not yet been submitted as part of (what the department referred to as) a library. This required implementing an API for the web application server to query the configuration management system's database remotely.

- I was also assigned the task of migrating the compiling methodology for creating a build from the use of SDL tools to pure GCC. This involved understanding the manner in which the source code that the department produced was compiled and how some of the C/C++ code was generated. This task gave me a great overview of the workflow for producing professional software and also extended my knowledge and skills for compiling C/C++ code, including the importance of makefiles and how they should be produced.


## Interests

- Software development is a hobbyist passion of mine as well as a career. One of my projects is a minimal RESTful application framework in PHP (<https://github.com/shroophp/framework>), which makes use of multiple design patterns including singletons, object pools and template methods. Another is a content management system that is built on top of the framework (<https://github.com/shroophp/cms>). I am currently working on a C implementation of the board game known as Blokus (<https://github.com/JontyNewman/libblokus>), which I intend to be the core functionality of any related applications I build regardless of their associated platform.

- I am very much a musical person and take great pleasure in writing, recording and mixing music as well as playing bass guitar and singing. This enjoyment resulted in me forming a band with my close friend. This has been a great way of learning how to collaborate with others and understand what someone is trying to communicate, especially when it's something as abstract as sound. You can listen to songs we have written and recorded together via our SoundCloud profile at <https://soundcloud.com/heatsignature>.


## References

### Charles Duncombe

- Director of Holidaysplease Ltd
- Church Court, 11-12 Cox Street, Birmingham, B3 1RD
- +44 (0)8453 657 000; charles.duncombe@holidaysplease.com

### Duncan Evemy

- Recruitment Manager, Mentor at BlackBerry Ltd
- +44 (0)7707 - 191997; duncan@evemy.com

