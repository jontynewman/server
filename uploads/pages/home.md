# Hello World!

I'm Jonty Newman. I'm a software engineering BEng graduate of [Aberystwyth
University](http://aber.ac.uk/). I spend a lot of my free time writing code.
When I'm not programming, I'm either playing bass guitar, recording music or
visiting a theme park.

This website is one of my many hobbyist projects. It is a result of me falling
in love with [Ruby On Rails](http://rubyonrails.org/) during an internship with
[BlackBerry Ltd](http://blackberry.com/). After becoming familiar with the
features, I wanted to know the challenges involved in implementing similar
functionality as a [PHP](http://php.net/) library. Once I had a stable codebase,
I was then curious to understand the challenges involved in implementing a
content management system that depended on this newly created framework.

This endeavour has resulted in me creating two
[Composer](http://getcomposer.org/) packages, namely
[ShrooPHP\Framework](http://github.com/shroophp/framework) and
[ShrooPHP\CMS](http://github.com/shroophp/cms). Both of these are used to
maintain this website. Sorry that there isn't much content here at the moment. I
hope to be able to update it soon. Watch this space!
